const { Users } = require('../models')
const jwt = require('jsonwebtoken')

const refreshToken = async (req, res) => {
    try {
        const refreshToken = req.cookies.refreshToken

        if (!refreshToken) return res.sendStatus(401)
        const user = await Users.findOne({
            where: {
                refresh_token: refreshToken
            }
        })

        if (!user) return res.sendStatus(403)
        jwt.verify(refreshToken, process.env.REFRESH_TOKEN_SECRET, (error,decoded) => {
            if (error) return res.sendStatus(403)

            const { id, name, email } = user

            const accessToken = jwt.sign({id, name, email}, process.env.ACCESS_TOKEN_SECRET, {
                expiresIn: '30s'
            })

            res.json({
                accessToken
            })
        })
    } catch (error) {
        console.log(error);
    }
}

module.exports = refreshToken